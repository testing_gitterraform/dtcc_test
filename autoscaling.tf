resource "aws_launch_template" "web_server_lt" {
  name_prefix   = "web-server-lt"
  image_id      = var.ami_id
  instance_type = var.web_server_instance_type
  key_name      = var.key_name

  network_interfaces {
    associate_public_ip_address = true
    security_groups             = [aws_security_group.web_sg.id]
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "Web Server"
    }
  }
}

resource "aws_autoscaling_group" "web_server_asg" {
  name_prefix = "web-server-asg"
  min_size    = 1
  max_size    = 3
  desired_capacity = 2

  launch_template {
    id      = aws_launch_template.web_server_lt.id
    version = "$Latest"
  }

  vpc_zone_identifier = [aws_subnet.public_az1.id, aws_subnet.public_az2.id]

  target_group_arns = [aws_lb_target_group.test_tg.arn]

  tag {
    key                 = "Name"
    value               = "Web Server Autoscaling"
    propagate_at_launch = true
  }
}
#-------------------------------------------------------------------------------------
resource "aws_launch_template" "app_server_lt" {
  name_prefix   = "app-server-lt"
  image_id      = var.ami_id
  instance_type = var.app_server_instance_type
  key_name      = var.key_name

  network_interfaces {
    associate_public_ip_address = false
    security_groups             = [aws_security_group.app_sg.id]
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "App Server"
    }
  }
}

resource "aws_autoscaling_group" "app_server_asg" {
  name_prefix = "app-server-asg"
  min_size    = 1
  max_size    = 3
  desired_capacity = 2

  launch_template {
    id      = aws_launch_template.app_server_lt.id
    version = "$Latest"
  }

  vpc_zone_identifier = [aws_subnet.private_az1.id, aws_subnet.private_az2.id]

  target_group_arns = [aws_lb_target_group.test_tg.arn]

  tag {
    key                 = "Name"
    value               = "App Server Autoscaling"
    propagate_at_launch = true
  }
}
