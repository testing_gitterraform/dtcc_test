variable "ami_id" {
  description = "The ID of the Amazon Machine Image (AMI)"
  type        = string
  default     = "ami-0ba259e664698cbfc"
}

variable "region" {
  description = "AWS region"
  default     = "ap-south-1"
}

variable "vpc_cidr" {
  description = "The CIDR block for the VPC."
  type        = string
  default     = "10.10.0.0/16"
}

variable "public_subnet_cidr_block_az1" {
  description = "CIDR block for the public subnet in AZ1"
  default     = "10.10.1.0/24"
}

variable "private_subnet_cidr_block_az1" {
  description = "CIDR block for the private subnet in AZ1"
  default     = "10.10.2.0/24"
}

variable "public_subnet_cidr_block_az2" {
  description = "CIDR block for the public subnet in AZ2"
  default     = "10.10.3.0/24"
}

variable "private_subnet_cidr_block_az2" {
  description = "CIDR block for the private subnet in AZ2"
  default     = "10.10.4.0/24"
}

variable "db_subnet_cidr_block_az1" {
  description = "CIDR block for the DB subnet in AZ1"
  default     = "10.10.5.0/24"
}

variable "db_subnet_cidr_block_az2" {
  description = "CIDR block for the DB subnet in AZ2"
  default     = "10.10.6.0/24"
}

variable "web_server_instance_type" {
  description = "Instance type for the web server"
  default     = "t2.micro"
}

variable "app_server_instance_type" {
  description = "The instance type for the application server"
  default     = "t2.medium"
}

variable "db_server_instance_type" {
  description = "The instance type for the database server"
  default     = "t2.medium"
}

variable "instance_count" {
  description = "The number of instances to create."
  type        = number
  default     = 1
}

variable "key_name" {
  description = "The name of the key pair to use for EC2 instances"
  type        = string
  default     = "manual-test"
}

