resource "aws_eip" "nat_eip_az1" {
  domain = "vpc"

  tags = {
    Name = "Dtcc-eip-az1"
  }
}

resource "aws_eip" "nat_eip_az2" {
  domain = "vpc"

  tags = {
    Name = "Dtcc-eip-az2"
  }
}

